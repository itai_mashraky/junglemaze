using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndButtonScript : MonoBehaviour
{
    public Camera PlayerCamera;
    public bool IsPlayerInRange;
    public float ActivationRange;
    RaycastHit hit;
    public LayerMask LayerToIgnore;

    // Start is called before the first frame update
    void Start()
    {
        PlayerCamera = GameObject.FindGameObjectWithTag("Player_Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, PlayerCamera.transform.parent.transform.position) < ActivationRange)
        {
            if (Physics.Raycast(PlayerCamera.transform.position, PlayerCamera.transform.forward, out hit, ActivationRange, ~LayerToIgnore))
            {
                if (hit.collider.CompareTag("EndButton") && Input.GetKeyDown(KeyCode.E))
                {
                    Cursor.lockState = CursorLockMode.None;
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                }

            }
        }
    }
}
