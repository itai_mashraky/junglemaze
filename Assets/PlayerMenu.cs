using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerMenu : MonoBehaviour
{
    public GameObject PauseMenu, LoseMenu, infoScreen;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (infoScreen.activeSelf && Input.GetKeyDown(KeyCode.E))
        {
            infoScreen.SetActive(false);
        }
        if (DataHolder.CanPlay())
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                PauseGame();
            }
            if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().HP <= 0)
            {
                Time.timeScale = 0;
                LoseMenu.SetActive(true);
                Cursor.lockState = CursorLockMode.None;

            }
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        PauseMenu.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void restartLevel()
    {
        DataHolder.IsGameRunnging = false;
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Cursor.lockState = CursorLockMode.None;
    }
    public void mainMenu()
    {
        DataHolder.IsGameRunnging = false;
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
        Cursor.lockState = CursorLockMode.None;

    }
}
