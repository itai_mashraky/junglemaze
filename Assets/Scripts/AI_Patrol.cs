using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

public class AI_Patrol : Simple_AI_Agent
{
    public GameObject[] Targets;
    public float RotationSpeed, FireRange, TimeBetweenShots, TimeBetweenShotsCounter;
    public bool RotatRight, AtPost, PlayerInRange;
    public GameObject Projectile;

    // Start is called before the first frame update
    void Start()
    {
        Agent = GetComponent<NavMeshAgent>();


        Agent.SetDestination(Targets[CurrentTaragetIndex++].transform.position);

        Projectile = Resources.Load("Projectile") as GameObject;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (DataHolder.IsGameRunnging)
        {
            if (DataHolder.PlayerSpotted_1)
            {
                Agent.SetDestination(transform.position);
                if (Vector3.Distance(transform.position, player.position) > FireRange)
                {
                    Agent.SetDestination(player.position);
                    PlayerInRange = false;
                }
                else
                {
                    PlayerInRange = true;
                    Agent.SetDestination(transform.position);
                }
                if (PlayerInRange && TimeBetweenShotsCounter <= 0)
                {
                    Instantiate(Projectile, transform.position, Quaternion.identity);
                    TimeBetweenShotsCounter = TimeBetweenShots;
                }
                TimeBetweenShotsCounter -= Time.deltaTime;
            }
            else
            {
                FOV();
                if (RayHit)
                {
                    if (HitPlayer)
                    {
                        if (PlayerInAngle)
                        {
                            DataHolder.PlayerSpotted_1 = true;
                        }
                    }
                }
                if (Agent.remainingDistance < 1 && !AtPost)
                {
                    CurrentViewAngle = transform.rotation.eulerAngles.y;
                    AtPost = true;
                }
                if (AtPost)
                {
                    ArrivedToPost();
                }

            }
        }
    }

    public void ArrivedToPost()
    {
        if (RotatRight)
        {
            transform.Rotate(Vector3.up * RotationSpeed * Time.deltaTime);
        }
        else
        {
            transform.Rotate(Vector3.up * -RotationSpeed * Time.deltaTime);
        }
        if (transform.rotation.eulerAngles.y >= CurrentViewAngle + 30 && RotatRight)
        {
            RotatRight = false;
        }
        if (CurrentViewAngle > 30)
        {
            if (transform.rotation.eulerAngles.y <= CurrentViewAngle - 30 && !RotatRight)
            {
                GoToNextPost();
            }
        }
        else
        {
            if (transform.rotation.eulerAngles.y > 350 && !RotatRight)
            {
                GoToNextPost();
            }
        }
    }
    public void GoToNextPost()
    {
        AtPost = false;
        RotatRight = true;
        if (CurrentTaragetIndex == Targets.Length)
        {
            CurrentTaragetIndex = 0;
            Agent.SetDestination(Targets[CurrentTaragetIndex].transform.position);
        }
        else
        {
            Agent.SetDestination(Targets[CurrentTaragetIndex++].transform.position);
        }

    }
    public int SortByName(GameObject a, GameObject b)
    {
        return a.name.CompareTo(b.name);
    }

}
