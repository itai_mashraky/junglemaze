using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillBoard : MonoBehaviour
{
    [SerializeField] private Transform Camera;

    private void Start()
    {
        Camera = GameObject.Find("Player_Camera").transform;
    }

    private void LateUpdate()
    {
        transform.LookAt(transform.position + Camera.forward);
    }

}
