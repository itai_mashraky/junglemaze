using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public DoorAnimationController DoorAnimationControllerScript;
    public Camera PlayerCamera;
    public bool IsPlayerInRange, IsRamp;
    public float ActivationRange;
    RaycastHit hit;
    public LayerMask LayerToIgnore;

    // Start is called before the first frame update
    void Start()
    {
        PlayerCamera = GameObject.FindGameObjectWithTag("Player_Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, PlayerCamera.transform.parent.transform.position) < ActivationRange)
        {
            IsPlayerInRange = true;
            if (Physics.Raycast(PlayerCamera.transform.position, PlayerCamera.transform.forward, out hit, ActivationRange, ~LayerToIgnore))
            {
                if (hit.collider.CompareTag("Button") && Input.GetKeyDown(KeyCode.E))
                {
                    if (IsRamp)
                    {
                        DoorAnimationControllerScript.ChangeRampState();
                    }
                    else
                    {
                        DoorAnimationControllerScript.ChangeOpenState();
                    }
                }

            }
        }
        else
        {
            IsPlayerInRange = false;
        }
    }


}
