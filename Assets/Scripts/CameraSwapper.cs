using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraSwapper : MonoBehaviour
{
    public bool PlayerCameraActive = true;
    public GameObject PlayerCamera;
    public GameObject[] SecurityCameras;
    public int ActiveSecurityCameraIndex = 0;
    public GameObject SecurityCanvas;
    public Text CameraDisplayText;

    // Start is called before the first frame update
    void Start()
    {
        PlayerCamera = GameObject.FindGameObjectWithTag("Player_Camera");
        SecurityCameras = GameObject.FindGameObjectsWithTag("SecurityCamera");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            Switch_player_security();
        }
        if(Input.GetKeyDown(KeyCode.P) && !PlayerCameraActive)
        {
            NextCamera();
        }
    }

   public void Switch_player_security()
    {
        if (PlayerCameraActive)
        {
            DataHolder.IsPlayerActive = false;
            SecurityCanvas.SetActive(true);
            PlayerCameraActive = false;
            PlayerCamera.GetComponent<Camera>().enabled = false;
            SecurityCameras[ActiveSecurityCameraIndex].GetComponent<Camera>().enabled = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            DataHolder.IsPlayerActive = true;
            SecurityCanvas.SetActive(false);
            PlayerCameraActive = true;
            PlayerCamera.GetComponent<Camera>().enabled = true;
            SecurityCameras[ActiveSecurityCameraIndex].GetComponent<Camera>().enabled = false;
            Cursor.lockState = CursorLockMode.Locked;

        }

    }

   public void NextCamera()
    {
        SecurityCameras[ActiveSecurityCameraIndex].GetComponent<Camera>().enabled = false;
        ActiveSecurityCameraIndex++;
        if(ActiveSecurityCameraIndex == SecurityCameras.Length)
        {
            ActiveSecurityCameraIndex = 0;
        }
        CameraDisplayText.text = "Camera#:" + ActiveSecurityCameraIndex;
        SecurityCameras[ActiveSecurityCameraIndex].GetComponent<Camera>().enabled = true;
    }
}
