using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataHolder
{
    public static bool IsGameRunnging = false;
    public static bool PlayerSpotted_1 = false;
    public static bool IsPlayerActive = true;

    public static bool CanPlay()
    {
        return IsPlayerActive && IsGameRunnging;
    }
}
