using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimationController : MonoBehaviour
{
    public string IsOpenString, IsRampString;
    public bool IsOpen, IsRamp;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ChangeOpenState()
    {
        IsOpen = !IsOpen;
        animator.SetBool(IsOpenString, IsOpen);
    }
    public void ChangeRampState()
    {
        IsRamp = !IsRamp;
        animator.SetBool(IsRampString, IsRamp);
    }

    
}
