using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_HP : MonoBehaviour
{
    public HealthBar HealthBar;
    public int hp = 100;

    // Start is called before the first frame update
    void Start()
    {
        HealthBar.SetMaxHealth(hp);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void takeDamage(int damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            Destroy(this.gameObject);
        }
        HealthBar.SetHealth(hp);
    }

}
