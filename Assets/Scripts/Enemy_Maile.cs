using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Maile : MonoBehaviour
{
    public int damage;
    public bool PlayerInRange;
    public Transform Player;
    public float TimeBetweenHits, timeBetweenHitsCounter;


    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, Player.position) <= 2)
        {
            PlayerInRange = true;
        }
        else
        {
            PlayerInRange = false;
        }
        if(PlayerInRange && timeBetweenHitsCounter <= 0)
        {
            Player.GetComponent<PlayerMovement>().TakeDamage(damage);
            timeBetweenHitsCounter = TimeBetweenHits;
        }
        timeBetweenHitsCounter -= Time.deltaTime;
    }

    
}
