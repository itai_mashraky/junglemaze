using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour
{
    public Transform Camera;

    public float ReloadTime, MPR, MPRCounter, ReloadCounter;

    public int MagCounter, OverAllBulletCount, MagSize, MaxOverAllBulletCount, Damage;

    public bool Firing, EmptyMag;

    public string GunMame;

    public Text AmmoGunDisplay;

    public ParticleSystem MuzzleFlash;

    public GameObject Impact;

    public LayerMask LayerToIgnore;

    private void OnEnable()
    {
        Camera = GameObject.FindGameObjectWithTag("Player_Camera").transform;
        SetWeapon();
    }
    // Update is called once per frame
    void Update()
    {
        if (DataHolder.CanPlay())
        {
            if (ReloadCounter > 0) // if timer is running
            {
                ReloadCounter -= Time.deltaTime; // reduce timer count
            }

            else if (ReloadCounter != -100) // if timer finished
            {
                Reload(); // call reload
            }

            if (MPRCounter > 0)//if the MPRCounter is above 0
            {
                MPRCounter -= Time.deltaTime * 1000;//subtracting time in Milisecond Method
            }

            if (Input.GetKeyDown(KeyCode.R) && OverAllBulletCount > 0)//if the user press  "R"(Reload) and there is bullets to reload
            {
                StartReload(); // start the reload timer
            }

            if (Input.GetMouseButtonDown(0) && ReloadCounter == -100 && !EmptyMag && MagCounter > 0)//if the player press the mouse and all fo the Parameters are true
            {
                Firing = true;//if true is firing
            }

            if (Input.GetMouseButtonUp(0) && Firing) // stop firing if released mouse button and firing
            {
                Firing = false; // set firing flag to false
            }

            if (Firing && MPRCounter <= 0 && MagCounter > 0)//if firig is true and enogh timepassed from the last shot and there is a bullet on the mag
            {

                Fire();//shots a bullet

                MPRCounter = MPR;//resets the MPRcounter

                if (MagCounter == 0 && OverAllBulletCount > 0) // if mag is empty, and has more bullets
                {
                    Firing = false; // stop firing
                    StartReload(); // start reloading
                }
            }

            AmmoGunDisplay.text = GunMame + " " + OverAllBulletCount + " / " + MagCounter;
        }
    }

    /// <summary>
    /// fires a bullet
    /// </summary>
    void Fire()
    {
        MuzzleFlash.Play();
        RaycastHit Hit; // saves the raycast data
        if (Physics.Raycast(Camera.transform.position, Camera.transform.forward, out Hit, float.MaxValue, ~LayerToIgnore)) // if hit something (sending raycast from the camera forward)
        {
            GameObject ImpactGO = Instantiate(Impact, Hit.point, Quaternion.LookRotation(Hit.normal));
            Destroy(ImpactGO, 2f);
            if (Hit.collider.CompareTag("Enemy"))
            {
                Hit.collider.GetComponent<Enemy_HP>().takeDamage(Damage);
            }
        }
        if (MagCounter - 1 == 0)//every time the player shots a bullet, subtracting 1 from the mag
            MagCounter = 0; // reduce mag count
        else
            MagCounter--; // ^^
    }

    /// <summary>
    /// start the reload timer
    /// </summary>
    void StartReload()
    {
        ReloadCounter = ReloadTime;//starts the reload timer
    }

    /// <summary>
    /// finish the reload
    /// </summary>
    void Reload()
    {
        ReloadCounter = -100; // reset finished reload counter
        if (OverAllBulletCount == 0)//if the magazine is empty
        {
            EmptyMag = true;//if true sets the mag and ammo count to empty
            return;//stops the reload process
        }
        if (OverAllBulletCount - (MagSize - MagCounter) <= 0)//if the reload will end the overallcount
        {
            MagCounter += (int)OverAllBulletCount;//refiling the mag
            OverAllBulletCount = 0;//sets the overall to 0
            return;//ending the reload process
        }
        OverAllBulletCount -= (MagSize - MagCounter);//subtracting from the overall
        MagCounter = MagSize;//puts the new mag count
    }


    public void SetWeapon()
    {
        OverAllBulletCount = MaxOverAllBulletCount;
        OverAllBulletCount -= MagSize;
        MagCounter = MagSize;
        EmptyMag = false;
    }
}
