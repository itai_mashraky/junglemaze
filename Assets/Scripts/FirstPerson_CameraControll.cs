using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPerson_CameraControll : MonoBehaviour
{
    public float Sensitivity = 100f;//Mouse sensitivity
    float Xrotation = 0f;//the rotation of the camera


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (DataHolder.CanPlay())
        {
            float MouseX = Input.GetAxis("Mouse X") * Sensitivity * Time.deltaTime; //The mouse input for the rotation on the X line
            float MouseY = Input.GetAxis("Mouse Y") * Sensitivity * Time.deltaTime; //The mouse input for the rotation on the Y line

            Xrotation -= MouseY;//A method to prevant Clamping(turning the head 360 degrees) (marked with *)
            Xrotation = Mathf.Clamp(Xrotation, -90f, 90f);//*

            transform.localRotation = Quaternion.Euler(Xrotation, 0f, 0f);//sets the camera view position


            transform.parent.Rotate(Vector3.up * MouseX);//rotates the player body according to the look
        }
    }
}
