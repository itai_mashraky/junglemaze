﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunView : MonoBehaviour
{
    Vector3 baseScale = new Vector3(0.7f, 0.7f, 0.7f);
    bool  isFirst = true;
    private void OnEnable()
    {
        transform.localScale = baseScale;
    }
    void Update()
    {


        if (Input.GetMouseButton(0))
        {
            if (isFirst)
            {
                isFirst = false;
                return;
            }
            ScaleUp();
        }
        else
        {
            isFirst = true;
            ScaleDown();
        }

    }
    void ScaleUp()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, 0.075f);
    }
    void ScaleDown()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, baseScale, 0.075f);
    }
}
