﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunsMenu : MonoBehaviour
{
    public GameObject Buttons;
    public GameObject[] GunsPreview;
    public GameObject[] GunsDesc;
    public GameObject[] GunsInHands;
    public GameObject CrossHair;
    public int currentGun = 0;

    void Start()
    {
        Time.timeScale = 0;
        GunsPreview[0].SetActive(true);
        GunsDesc[0].SetActive(true);
    }

    public void NextGun()
    {
        GunsPreview[currentGun].SetActive(false);
        GunsDesc[currentGun].SetActive(false);
        currentGun--;
        if (currentGun < 0)
            currentGun = GunsPreview.Length - 1;
        GunsPreview[currentGun].SetActive(true);
        GunsDesc[currentGun].SetActive(true);

    }
    public void ChooseGun()
    {
        CrossHair.SetActive(true);
        Time.timeScale = 1;
        DataHolder.IsGameRunnging = true;
        Cursor.lockState = CursorLockMode.Locked;
        Buttons.SetActive(false);
        GunsInHands[currentGun].SetActive(true);
        for (int i = 0; i < GunsPreview.Length; i++)
        {
            Destroy(GunsPreview[i]);
            Destroy(GunsDesc[i]);
        }
    }
}
