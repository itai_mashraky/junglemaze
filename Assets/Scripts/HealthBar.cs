﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider Slider;//a veriable for the HealthBar Slider
    public Gradient Gradient;//a veriable for the changing color Method
    public Image Fill;//the HealthBar fill


    public void SetMaxHealth(int Health)//sets the max health 
    {
        Slider.maxValue = Health;//sets the max value
        Slider.value = Health;//sets the slider max value

        Fill.color = Gradient.Evaluate(1f);
    }

    public void SetHealth(int Health)//sets the new health 
    {
        Slider.value = Health;
        Fill.color = Gradient.Evaluate(Slider.normalizedValue);//sets the new HealthBar fill if the user takes damage
    }

}
