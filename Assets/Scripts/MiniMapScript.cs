using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapScript : MonoBehaviour
{
    public Transform Player;

    private void LateUpdate()
    {
        FollowPlayerPos();
        FollowPlayerRot();
    }

    void FollowPlayerPos()
    {
        Vector3 NewPos = Player.position;
        NewPos.y = transform.position.y;
        transform.position = NewPos;
    }

    void FollowPlayerRot()
    {
        transform.rotation = Quaternion.Euler(90f, Player.eulerAngles.y, 0f);
    }
}
