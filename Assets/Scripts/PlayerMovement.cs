using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController Controller;
    private float FloatX, FloatZ ,gravity = -9.81f;
    public float Speed, GroundCheckDistance;
    private Vector3 move, velocity;
    public int HP;
    public HealthBar HBS;
    public bool IsGrounded;

    // Start is called before the first frame update
    void Start()
    {
        Controller = gameObject.GetComponent<CharacterController>();
        HBS.SetMaxHealth(HP);
    }

    // Update is called once per frame
    void Update()
    {
        if (DataHolder.CanPlay())
        {
            IsGrounded = Physics.Raycast(transform.position - Vector3.up, Vector3.down, GroundCheckDistance);

            if(IsGrounded && velocity.y < 0)
            {
                velocity = Vector3.zero;
            }
            if (!IsGrounded)
            {
                velocity.y += gravity * Time.deltaTime;
            }

            FloatX = Input.GetAxis("Horizontal") * Speed;
            FloatZ = Input.GetAxis("Vertical") * Speed;

            move = transform.right * FloatX + transform.forward * FloatZ;

            Controller.Move(move * Time.deltaTime);
            Controller.Move(velocity * Time.deltaTime);
        }
    }

    public void TakeDamage(int damage)
    {
        HP -= damage;
        HBS.SetHealth(HP);
    }
    public void TakeHP(int HP)
    {
        this.HP += HP;
        this.HP = Mathf.Clamp(this.HP, 0, 100);
        HBS.SetHealth(this.HP);
    }
}


