using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUp : MonoBehaviour
{
    public bool Type; //true for health false for Ammo
    public float rotationSpeed;
    

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Type)
            {
                other.GetComponent<PlayerMovement>().TakeHP(50);
            }
            else
            {
                other.GetComponentInChildren<FPS>().OverAllBulletCount = other.GetComponentInChildren<FPS>().MaxOverAllBulletCount - other.GetComponentInChildren<FPS>().MagSize;
                other.GetComponentInChildren<FPS>().MagCounter = other.GetComponentInChildren<FPS>().MagSize;
               
            }
            Destroy(this.gameObject);
        }
    }
}

