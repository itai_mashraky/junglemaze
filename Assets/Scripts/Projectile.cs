using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public GameObject Player;
    public Vector3 Target;
    public int speed, Damage;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        Target = Player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, Target, speed * Time.deltaTime);
        if (Vector3.Distance(transform.position, Target) == 0)
        {
            DestroySelf();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Player.GetComponent<PlayerMovement>().TakeDamage(Damage);
            DestroySelf();
        }
    }

    void DestroySelf()
    {
        Destroy(this.gameObject);
    }
}
