using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

public class Simple_AI_Agent : MonoBehaviour
{
    public NavMeshAgent Agent;
    public int CurrentTaragetIndex = 0;
    public float maxRadius, maxAngle, CurrentViewAngle;
    public bool isInFOV, RayHit, HitPlayer, PlayerInAngle;
    public Transform player;
    private RaycastHit hit;

    // Start is called before the first frame update
    void Start()
    {
        Agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        FOV();
        if (isInFOV)
        {
            if (Vector3.Distance(player.position, transform.position) > 2)
            {
                Agent.SetDestination(player.position);
            }
            else
            {
                Agent.SetDestination(transform.position);
            }
        }
    }

    protected void FOV()
    {
        Vector3 DirectionBetween = (player.position - transform.position).normalized;
        if (Physics.Raycast(transform.position, DirectionBetween, out hit, maxRadius))
        {
            RayHit = true;
            if (LayerMask.LayerToName(hit.transform.gameObject.layer) == "Player")
            {
                HitPlayer = true;
                float angle = Vector3.Angle(transform.forward, DirectionBetween);

                if (angle <= maxAngle)
                {
                    isInFOV = true;
                    PlayerInAngle = true;
                }
                else
                {
                    isInFOV = false;
                    PlayerInAngle = false;
                }
            }
            else
            {
                HitPlayer = false;
                isInFOV = false;
            }
        }
        else
        {
            RayHit = false;
        }

    }

    protected void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;//A color for the Radius
        Gizmos.DrawWireSphere(transform.position, maxRadius);//Draw the radius with a yellow color 

        Vector3 fovLine1 = Quaternion.AngleAxis(maxAngle, transform.up) * transform.forward * maxRadius;//sets the fov aspect, with the radius and the look angel(marked with !)
        Vector3 fovLine2 = Quaternion.AngleAxis(-maxAngle, transform.up) * transform.forward * maxRadius;//!
        Vector3 fovLine3 = Quaternion.AngleAxis(-maxAngle, transform.right) * transform.forward * maxRadius;//!
        Vector3 fovLine4 = Quaternion.AngleAxis(maxAngle, transform.right) * transform.forward * maxRadius;//!

        Gizmos.color = Color.blue;//sets the FOV lines coloer to blue
        Gizmos.DrawRay(transform.position, fovLine1);//draws the FOV aspect on the screen @
        Gizmos.DrawRay(transform.position, fovLine2);//@
        Gizmos.DrawRay(transform.position, fovLine3);//@
        Gizmos.DrawRay(transform.position, fovLine4);//@

        if (!isInFOV)
            Gizmos.color = Color.red;
        if (isInFOV)
            Gizmos.color = Color.green;//the player position ray color
        Gizmos.DrawRay(transform.position, (player.position - transform.position).normalized * maxRadius);//draws a line to the player position

        Gizmos.color = Color.black;//the forward ray color
        Gizmos.DrawRay(transform.position, transform.forward * maxRadius);//draws a ray to the AI forward direction
    }

}
